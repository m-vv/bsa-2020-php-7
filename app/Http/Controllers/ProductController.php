<?php

namespace App\Http\Controllers;

use App\Entity\Product;

use App\Http\Resources\ProductResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('viewAny', Product::class);
        } catch (AuthorizationException $e) {
            return  redirect('/');
        }
        $products = Product::all();
        return view('product.products', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $this->authorize('create', Product::class);
        } catch (AuthorizationException $e) {
            return  redirect('/products');
        }
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->authorize('create', Product::class);
        } catch (AuthorizationException $e) {
            return  redirect('/');
        }
        $product = new Product;
        $product->name = $request['name'];
        $product->price = $request['price'];
        $product->user_id =  $request->user()->id;
        $product->save();
        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  Product  $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Product $product)
    {
        try {
            $this->authorize('view', $product);
        } catch (AuthorizationException $e) {
            return  redirect('/');
        }
        return view('product.product', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        try {
            $this->authorize('update', $product);
        } catch (AuthorizationException $e) {
            return  redirect('/products');
        }
        return view('product.update', ['product'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            $this->authorize('update', $product);
        } catch (AuthorizationException $e) {
            return  redirect('/products');
        }
        $product->name = $request['name'];
        $product->price = $request['price'];
        $product->user_id =  $request['user_id'];
        $product->save();
        return redirect('/products/'.$product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $this->authorize('delete', $product);
        } catch (AuthorizationException $e) {
            return  redirect('/products');
        }
        Product::destroy($product->id);
        return redirect('/products/');
    }

    /**
     * Show the form for delete the specified resource.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function showDeleteForm(Product $product)
    {
        try {
            $this->authorize('delete', $product);
        } catch (AuthorizationException $e) {
            return redirect('/products');
        }
            return view('product.delete', ['product' => $product]);
    }
}
