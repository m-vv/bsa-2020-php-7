@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3>List of all products</h3>
                <table class="table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>price</th>
                        <th>user name</th>
                    </tr>
                    </thead>
                    <tbody class = "text-body">
                    @foreach( $products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td><a href="/products/{{$product->id}}">{{$product->name}}  </a></td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->user->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @can('create', $product)
                    <a href = "{{route('products.create')}}">Add</a>
                @endcan

            </div>
        </div>
    </div>
@endsection
