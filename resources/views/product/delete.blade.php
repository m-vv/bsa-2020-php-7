@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h2>Please see details about product you want to delete</h2></div>
                    <div class="card-body">
                        <div class="card-body">
                            <ul>
                                <li> id: {{$product->id}}</li>
                                <li> name:  {{$product->name}}</li>
                                <li> price: {{$product->price}}</li>
                                <li> user name: {{$product->user->name}}</li>
                            </ul>
                        </div>
                         <form method="POST" action="{{route('products.destroy', ['product' =>$product->id ]) }}">
                            @csrf
                            @method('DELETE')
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Delete') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="card-footer"> <a href = '/products'>All products</a></div>
                </div>
            </div>
        </div>
    </div>
@endsection
