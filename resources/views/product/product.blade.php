@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Details about product</div>
                    <div class="card-body">
                        <ul>
                            <li> id: {{$product->id}}</li>
                            <li> name:  {{$product->name}}</li>
                            <li> price: {{$product->price}}</li>
                            <li> user name: {{$product->user->name}}</li>
                        </ul>
                    </div>
                    <div class="card-footer">
                        @can('delete', $product)
                            <form method="POST" action="{{route('products.destroy', ['product' =>$product->id ]) }}">
                                @csrf
                                @method('DELETE')
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Delete') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endcan
                        @can('update', $product)
                            <a href = "{{route('products.edit', ['product' =>$product->id ]) }}"  class="btn btn-primary">Edit</a>
                        @endcan
                         @can('create', $product)
                            <a href = "{{route('products.create')}}" class="btn btn-primary">Add</a>
                         @endcan
                        <a href = "{{route('products.index') }}" class="btn btn-primary"> All products </a>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
