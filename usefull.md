# Installation
Remember to remove old containers
```shell script
docker-compose down
```
Check ports in host
```dockerfile
ports:
      - "33061:3306"
```
change .env
- database password
- correct DB_HOST=db

start composer 
```dockerfile
docker-compose exec app composer install
```
install code sniffers and mess detector via composer. Choose right version parameter. 

generate  key
```shell script
docker-compose exec app php artisan key:generate
```
install duck
````shell script
docker-compose exec app php artisan dusk:install
````

##Data migration
```shell script
docker-compose exec app php artisan make:model Product
```
! .env change DB_HOST=db   DB_PASSWORD=your_mysql_root_password
```shell script
docker-compose exec app php artisan migrate
```

now we can use to access phpmyadmin
```
http://localhost:9191
```

seeding of data
````shell script
 docker-compose exec app php artisan db:seed 
````
#Testing
to get site from host
```
http://localhost:8087/
```
to run tests


```shell script
docker-compose exec app php artisan dusk
```
#Authentication
##Basic
Problem command below does not work!
```shell script
docker-compose exec app php artisan make:auth
```
we should do
in laravel 7.x 
```shell script
docker-compose exec app composer require laravel/ui
```
in laravel ^6.2
```shell script
docker-compose exec app  composer require laravel/ui "^1.0" --dev
```


[see more](https://laravel.com/docs/6.x/authentication)

```shell script
docker-compose exec  app php artisan ui vue --auth
```
##Via github

```shell script
docker-compose exec app  composer require laravel/socialite 
```

# Controllers for Products 
Creating controller for  working with resources
```shell script
docker-compose exec  app php artisan make:controller ProductController --resource
```
 Working with presenters of product
 ```shell script
 docker-compose exec  app php artisan make:resource ProductResource
 ```
##Authorization
###Policy
``shell script
 docker-compose exec  app php artisan make:policy ProductPolicy --model=Product
 ```
